---
#background: "img/bg4.jpg"
title: "Terms of Use"

---

<h3>Disclaimer</h3>

No warranties, either express or implied, are hereby given for anything provided by Manjaro Linux ("Software"). All software is supplied without any accompanying guarantee, whether expressly mentioned, implied or tacitly assumed. This information does not include any guarantees regarding quality, does not describe any fair marketable quality, and does not make any claims as to quality quarantees or quarantees regarding the suitability for a special purpose. The user assumes all responsibility for damages resulting from the use of the software, including, but not limited to, frustration, disgust, system abends, disk head-crashes, general malfeasance, floods, fires, shark attack, locust infestation, cyclones, hurricanes, tsunamis, local electromagnetic disruptions, hydraulic brake system failure, invasion, hashing collisions, normal wear and tear of friction surfaces, cosmic radiation, inadvertent destruction of sensitive electronic components, windstorms, the riders of nazgul, infuriated chickens, premature activation of a distant early warning system, peasant uprisings, halitosis, artillery bombardment, explosions, cave-ins, borg-assimilation and/or frogs falling from the sky.

<h3>Free Software and Manjaro Linux</h3>

Manjaro develops free software. All our tools (Pamac, MSM, Manjaro-Tools) are and will always be free software. Manjaro Linux is part of <a href="http://www.openinventionnetwork.com/community-of-licensees/">OIN</a> since March 2014. The current license can be viewed <a href="http://www.openinventionnetwork.com/joining-oin/oin-license-agreement/">here</a>. Additional information about packages covered by this license can be viewed <a href="http://www.openinventionnetwork.com/joining-oin/linux-system/">here</a>.

But it is not our goal to make Manjaro a distro free of proprietary software, like <a href="http://trisquel.info/">Trisquel</a> or <a href="http://www.gnewsense.org/">gNewSense</a>. While you can use Manjaro without non-free software, our repositories also contain non-free software you can install. So, while it will always be possible to have a Manjaro with free software only, we will ship any software we can as long as our users ask for it and our packagers are willing to prepare it for them.

If you want to check the license of a package, you can do so with Pacman. Also, check if there is a folder called <strong><em>/usr/share/licenses/<package name=""></package></em></strong> in your system; if so, it should contain license information for that package.

Our live medias give you the choice to boot with free and non-free drivers, so you can check before installing which one you want to use.

<h3>Acknowledgements</h3>

We would like to thank everyone who has enabled us to create Manjaro, and/or contributed to it. These are in detail:
<ul>
 	<li>The Arch Linux <a href="http://www.archlinux.org/developers/">developers</a> and <a href="http://bbs.archlinux.org/">community</a>, for creating the perfect™ distribution.</li>
 	<li>The Pacman developers, for creating the fraggin` best package manager in the known universe.</li>
 	<li>The XFCE Project, for their lightweight desktop environment we use as default.</li>
 	<li>The KDE Project, for creating the most powerful desktop environment on planet earth.</li>
 	<li>The Gnome Project, for another great desktop environment.</li>
 	<li>Michael Towers (gradgrind) for creating the awesome <a href="http://larch.berlios.de/">larch</a> liveCD scripts</li>
 	<li>All people providing download and package mirrors. Without you we won't be here.</li>
 	<li>All testers who took the mission to test our stuff with patience.</li>
 	<li>All people actually reading documentation</li>
</ul>

<h3>Trademarks</h3>

Manjaro Linux uses several trademarks from different projects. Their rights are not overruled by our license and stay intact.
<ul>
 	<li>Manjaro Linux, copyright © 2011-2018 Philip Müller and the Manjaro Developers</li>
 	<li>Arch Linux, copyright © 2002-2018 Judd Vinet and Aaron Griffin</li>
 	<li>GNOME is licensed under the GNOME Foundation's License Agreement</li>
 	<li>KDE® and the K Desktop Environment® logo are registered trademarks of KDE e.V.</li>
 	<li>The registered trademark Linux® is used pursuant to a sublicense from LMI, the exclusive licensee of Linus Torvalds, owner of the mark on a world-wide basis.</li>
</ul>
All other trademarks and copyrights are property of their respective owners and are only mentioned for informative purposes.

<h3>Project representative</h3>
<p>Name: Philip Müller</br>
Organization: Manjaro Linux</br>
Street: Dr. Wintrichstr. 43</br>
City: Ebersberg</br>
State/Province: Bavaria</br>
Postal Code: 85560</br>
Country: Germany</br>
Email: <a href="mailto:support@manjaro.org">Direct contact</a></p>

<h3>Under the Hood</h3>

This site is powered by:
<p><a href="https://gohugo.io/">Hugo</a></br>
<a href="http://www.mediawiki.org">MediaWiki</a></br>
<a href="https://www.gitlab.com/">Gitlab</a></br>
<a href="https://www.discourse.org/">Discourse</a></br>
<a href="https://zammad.org/">Zammad</a></br>
<a href="http://www.list.org/">Mailman</a></p>
