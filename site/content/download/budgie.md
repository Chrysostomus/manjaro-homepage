+++
Download_x64 = "https://osdn.net/dl/manjaro-community/manjaro-budgie-18.0.1-stable-x86_64.iso"
Download_x64_Checksum = "a3971b70d3e2868f0a742a7e7dfcec967476e40e"
Download_x64_Sig = "https://osdn.net/dl/manjaro-community/manjaro-budgie-18.0.1-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro-community/manjaro-budgie-18.0.1-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = "https://sourceforge.net/projects/manjarotorrents/files/community/"
Name = "Budgie Edition"
Screenshot = "budgie-full.jpg"
ShortDescription = "This edition is supported by the Manjaro community and comes with Budgie, the desktop of the [Solus project](https://solus-project.com/). Budgie focuses on providing a simple-to-use and elegant desktop that fulfills the needs of a modern user."
Tags = [ "resourceefficient", "beginnerfriendly" ]
TargetGroup = "For people who want a simple and elegant desktop"
Thumbnail = "budgie.jpg"
Version = "18.0.1"
date = "23.12.2018T02:36:05+01:00"
title = "Budgie - Stable"
type="download-edition"
weight = "5"
+++

