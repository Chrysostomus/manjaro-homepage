+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-gnome-18.0.4-stable-x86_64.iso"
Download_x64_Checksum = "c7ce6949197a42b285425ae5c6c5203c883530c4"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-gnome-18.0.4-stable-x86_64.iso.sig"
Download_x64_Torrent = ""
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "GNOME Edition"
Screenshot = "gnome-full.jpg"
Youtube = "OjyfrFWaLDM"
ShortDescription = "This edition is supported by the Manjaro team and comes with the GNOME 3 desktop that breaks with traditional concepts and allows users to focus on their tasks. Desktop-specific applications are crafted with care and clearly defined by guidelines that make them more consistent to use."
Tags = [ "official", "beginnerfriendly" ]
TargetGroup = "For people who want a very modern and simple desktop"
Thumbnail = "gnome.jpg"
Version = "18.0.4"
date = "12.03.2019T12:37:00+01:00"
title = "Gnome - Stable"
type="download-edition"
weigth = "3"
+++

