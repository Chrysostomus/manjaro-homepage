+++
Download_x64 = "https://osdn.net/dl/manjaro-community/manjaro-deepin-18.0.2-stable-x86_64.iso"
Download_x64_Checksum = "abf369a95d5743fb07148ee309bcd4d8a24bb834"
Download_x64_Sig = "https://osdn.net/dl/manjaro-community/manjaro-deepin-18.0.2-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro-community/manjaro-deepin-18.0.2-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Deepin Edition"
Screenshot = "deepin-full.jpg"
ShortDescription = "This edition is supported by the Manjaro community and comes with the [Deepin](https://www.deepin.org/) desktop that provides a very beginner-friendly and elegant experience."
Tags = [ "resourceefficient", "beginnerfriendly" ]
TargetGroup = "For people who want a simple and elegant desktop"
Thumbnail = "deepin.jpg"
Version = "18.02"
date = "02.01.2019T11:21:18+01:00"
title = "Deepin - Stable"
type="download-edition"
+++

