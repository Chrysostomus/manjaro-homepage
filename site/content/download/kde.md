+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-kde-18.0.4-stable-x86_64.iso"
Download_x64_Checksum = "6de5434b8c8b68f402b69ceb69e5460125e2f0e2"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-kde-18.0.4-stable-x86_64.iso.sig"
Download_x64_Torrent = ""
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "KDE Edition"
Screenshot = "kde-full.jpg"
Youtube = "FHMBocs2Xc8"
ShortDescription = "This edition is supported by the Manjaro team and comes with KDE, a very modern and flexible desktop."
Tags = [ "official", "beginnerfriendly", "resourceefficient", "traditional_ui" ]
TargetGroup = "For people who want a modern and very flexible desktop"
Thumbnail = "kde.jpg"
Version = "18.0.4"
date = "12.03.2019T12:36:00+01:00"
title = "KDE - Stable"
type="download-edition"
weight = "2"
+++

KDE is a feature-rich and versatile desktop environment that provides several different styles of menu to access applications. An excellent built-in interface to easily access and install new themes, widgets, etc, from the internet is also worth mentioning. While very user-friendly and certainly flashy, KDE is also quite resource heavy and noticably slower to start and use than a desktop environment such as XFCE. A 64 bit installation of Manjaro running KDE uses about 550MB of memory.
