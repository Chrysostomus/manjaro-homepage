+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-xfce-18.0.4-stable-x86_64.iso"
Download_x64_Checksum = "2b01e9f4c8dccdb2096aebf5a18c7128e59ca3dd"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-xfce-18.0.4-stable-x86_64.iso.sig"
Download_x64_Torrent = ""
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "XFCE Edition"
Screenshot = "xfce-full.jpg"
Youtube = "zb_9tc-DiIs"
ShortDescription = "This edition is supported by the Manjaro team and comes with XFCE, a lightweight and reliable desktop with high configurability."
Tags = [ "official", "resourceefficient", "beginnerfriendly", "traditional_ui" ]
TargetGroup = "For people who want a reliable and fast desktop"
Thumbnail = "xfce.jpg"
Version = "18.0.4"
date = "12.03.2019T12:31:00+01:00"
title = "XFCE - Stable"
type="download-edition"
weight = "1"
+++

Xfce is a lightweight desktop environment for UNIX-like operating systems. It aims to be fast and low on system resources, while still being visually appealing and user friendly. Xfce embodies the traditional UNIX philosophy of modularity and re-usability. It consists of a number of components that provide the full functionality one can expect of a modern desktop environment. They are packaged separately and you can pick among the available packages to create the optimal personal working environment.

