+++
Download_x64 = "https://osdn.net/dl/manjaro/manjaro-architect-18.0.2-stable-x86_64.iso"
Download_x64_Checksum = "fa3740a6206ff45a46425cd73a8657125a7109e4"
Download_x64_Sig = "https://osdn.net/dl/manjaro/manjaro-architect-18.0.2-stable-x86_64.iso.sig"
Download_x64_Torrent = "https://osdn.net/dl/manjaro/manjaro-architect-18.0.2-stable-x86_64.iso.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Architect Edition"
Screenshot = "architect-full.jpg"
ShortDescription = "This edition is supported by the Manjaro team and comes with a TUI installer."
Tags = [ "official", "resourceefficient" ]
TargetGroup = "For people who want to setup and configure Manjaro from the ground up"
Thumbnail = "architect.jpg"
Version = "18.0.2"
date = "2019-02-17T11:38:00+01:00"
title = "Architect - Stable"
type="download-edition"
weight = "7"
+++

Manjaro-Architect is a fork of the famous Architect Linux installer by Carl Duff, that has been modified to install Manjaro instead of Arch Linux. It is a netinstaller that downloads always the latest packages, so your system is always up to date after installation, regardless of how old your install media is.

Manjaro-Architect offers currently unparalleled customization on your Manjaro installation:

- choose freely which kernel to use from all manjaro kernels. You can also choose multiple kernels.
- choose any manjaro branch (stable, testing or unstable)
- choose your desktop from all up to date manjaro editions, regardless of what install media you run the installer from. You can also choose to install just base system without graphical user interface.
- add any number of unconfigured desktop environments
- choose the default shell for user (bash, zsh or fish)
- choose your graphics drivers that are installed with mhwd
- choose arbitrary extra packages to be installed
